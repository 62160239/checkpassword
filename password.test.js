const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })

  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })

  test('should has alphabet Z in password', () => {
    expect(checkAlphabet('Z')).toBe(true)
  })

  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has digit 0 in password', () => {
    expect(checkDigit('0')).toBe(true)
  })

  test('should has digit 8 in password', () => {
    expect(checkDigit('8')).toBe(true)
  })

  test('should has not digit in password', () => {
    expect(checkDigit('zzzz')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol . in password', () => {
    expect(checkSymbol('11.11')).toBe(true)
  })

  test('should has symbol # in password', () => {
    expect(checkSymbol('Mj#')).toBe(true)
  })

  test('should has not symbol in password', () => {
    expect(checkSymbol('mmmm1')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password Kankan222# to be true', () => {
    expect(checkPassword('Kankan222#')).toBe(true)
  })

  test('should password Kankantida# to be false', () => {
    expect(checkPassword('Kankantida#')).toBe(false)
  })

  test('should password Kankantidaaditnakmayyam255# to be false', () => {
    expect(checkPassword('Kankantidaaditnakmayyam255#')).toBe(false)
  })
})
